# borne-macdo

## Fonctionnel

### Présentation fonctionnelle
Cette application est une borne de commande d'un restaurant. Elle permet de saisir des produits (catégorisés) dans la partie gestion de produits (espace réservé à l'administrateur) et de passer des commandes en sélectionnant les produits saisis par l'administrateur.
- L'administrateur bénéficie des fonctionnalités suivantes:
    - Créer un produit en saisissant son nom, son prix, sa catégorie et sa description
    - Modifier un produit (changer son nom, son prix, sa catégorie, sa description)
    - Supprimer un produit
- L'utilisateur pourra passer une commande depuis le menu commande en sélectionnant les produits de son choix.
    - Son interface lui permet de naviguer entre les catégories des produits.
    - Il peut voir les attributs de chaque produit.
    - Il pourra choisir ces derniers dans son panier autant de fois qu'il le souhaite.
    - Le prix de sa commande sera calculé de manière réactive (prix variable en temps réel en fonction de ses choix)

### TODO
- Espace panier: Boutton +- pour ajouter ou supprimer un produit du panier, calcul du prix en fonction des actions de l'utilisateur
- Dynamiser les image de chaque produit en ajoutant un champs image lors de la saisie des produits
- Permettre à l'administrateur de modifier le nom d'une catégorie et de supprimer une catégorie
- Relier l'application à un back office symfony pour enregistrer les produits dans une base de données
- Ajouter des tests unitaires

## Technique
L'application est réalisée en vuejs 2. Il fonctionne avec une surcouche de TypeScript qui vient ajouter le typage fort sur les variables.
La librairie front utilisée est vue material et l'application fonction avec le gestionnaire de store Vuex.

Le backoffice qui sera réalisée en Symfony 5 fournira des données à l'application à travers des apis. Les apis seront réalisées avec Api plateform. La base de données de stockage sera une base de données relationnelle MySQL.

## Prérequis
```
Docker desktop: https://docs.docker.com/desktop/ 
```
##### Mise en place de l'environnement
```
S'assurer que les ports 80, 8080 et 8081 sont disponible sinon faites la modifications sur le fichier docker-compose.yml
```
1. `git clone git@gitlab.com:elo03/borne-de-commande.git`
1. `cd docker-template` 
2. `make build-app`

##### Lancement des différentes applications
- Lancement du back:
    1. `cd docker-template`
    2. `make run-api-serve` pour lancer le serveur Symfony 5.

Le back est accessible depuis `http://localhost:80`

- Lancement du front
    1. `cd docker-template`
    2. `make run-front-serve` pour lancer le serveur (suceptible très longue la première fois)
    
Le back est accessible depuis `http://localhost:8080/`

- phpmyadmin

Pour suivre l'évolution de la base de données: `http://localhost:8081/` `user: root` et `password: root`

##### Commandes utiles

Toutes les commandes `make` doivent être exécuter depuis le fichier `docker-template` (`cd docker-template`)
- `make bash`: Accéder au bash du container.
- `make down`: Arreter toutes les images docker nécessaires à l'application
- `make rm`: Supprimer toutes les images docker nécessaires à l'application
- `make update-dependancies`: Mettre à jours les dépendances de symfony

Toutes les commandes sont disponibles depuis le fichier Makefile contenu dans docker-template

Après l'installation, rendez vous dans la partie `Gestion de produits` pour ajouter de nouveaux produits et enjoy!
## NB:
L'application continuera à être enrichie de nouvelles fonctionnalités jour en jours jusqu'à ce qu'elle soit complètement terminées. La partie design sera grandement améliorée au fur et à mesure.
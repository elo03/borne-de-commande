import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import Commander from "@/domain/vue/page/Commander.vue";
import Produit from "@/domain/vue/page/Produit.vue";
import Menu from "@/domain/vue/page/Menu.vue";

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: "/",
    name: "Commander",
    component: Commander
  },
  {
    path: "/gestion-des-produits",
    name: "Produits",
    component: Produit
  },
  {
    path: "/gestion-des-menus",
    name: "Menus",
    component: Menu
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;

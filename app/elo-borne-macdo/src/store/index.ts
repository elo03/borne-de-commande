import StoreInterface from "@/app/interface/StoreInterface";
import CategorieInterface from "@/domain/produits/interfaces/CategorieInterface";
import ProductInterface from "@/domain/produits/interfaces/ProductInterface";
import categorieList from "@/domain/produits/services/categorieProduct";
import Vue from "vue";
import Vuex, { Store, StoreOptions } from "vuex";

Vue.use(Vuex);
const store: StoreOptions<StoreInterface> = {
  state: {
    productList: [],
    categories: [],
    editingProduct: []
  },
  getters: {
    getProductList(state: StoreInterface): ProductInterface[] {
      return state.productList;
    },
    getCategorieList(state: StoreInterface): CategorieInterface[] {
      return state.categories;
    },
    getCategoryListName(state: StoreInterface): string[] {
      const categorieListName: string[] = [];
      state.categories.forEach((categorie: CategorieInterface) => {
        categorieListName.push(categorie.name);
      });
      return categorieListName;
    },
    getProductsListByCatgeory: (state: StoreInterface) => (
      categorie: CategorieInterface
    ) => {
      return state.productList.filter((product: ProductInterface) => {
        return product;
      });
    },
    getEditingProduct(state: StoreInterface): ProductInterface {
      return state.editingProduct;
    }
  },
  mutations: {
    setProductList(state: StoreInterface, product: ProductInterface): void {
      state.categories.filter((categorie: CategorieInterface) => {
        if (categorie.name === product.categorieSelected) {
          categorie.products.push(product);
        }
      });
    },
    setProductListOnMount(
      state: StoreInterface,
      product: ProductInterface[]
    ): void {
      product.forEach(element => {
        state.productList.push(element);
      });
    },
    setCategorieList(
      state: StoreInterface,
      categories: CategorieInterface[]
    ): void {
      state.categories = categorieList();
    },
    setEditingProduct(state: StoreInterface, product: ProductInterface): void {
      state.editingProduct = product;
    },
    addNewCategory(state: StoreInterface, categorie: CategorieInterface): void {
      state.categories.push(categorie);
    },
    updateProduct(state: StoreInterface, newProduct: ProductInterface) {
      state.categories.filter((category: CategorieInterface) => {
        if (category.name === newProduct.categorieSelected) {
          category.products.filter(p => {
            if (p.name === state.editingProduct.name) {
              p.name = newProduct.name;
              (p.description = newProduct.description),
                (p.price = newProduct.price);
            }
          });
        }
      });
    },
    updateProductMove(
      state: StoreInterface,
      newProduct: ProductInterface
    ): void {
      state.categories.filter((category: CategorieInterface) => {
        if (category.name === state.editingProduct.categorieSelected) {
          category.products.splice(
            category.products.indexOf(state.editingProduct),
            1
          );
        }
        if (category.name === newProduct.categorieSelected) {
          category.products.push(newProduct);
        }
      });
    },
    removeProductAndAddCategorie(
      state: StoreInterface,
      newCategory: CategorieInterface
    ): void {
      state.categories.filter((category: CategorieInterface) => {
        if (category.name === state.editingProduct.categorieSelected) {
          category.products.splice(
            category.products.indexOf(state.editingProduct),
            1
          );
        }
      });
      state.categories.push(newCategory);
    },
    removeProduct(state: StoreInterface, product: ProductInterface) {
      state.categories.filter((category: CategorieInterface) => {
        if (category.name === product.categorieSelected) {
          category.products.splice(category.products.indexOf(product), 1);
        }
      });
    }
  },
  actions: {},
  modules: {},
  strict: true,
  devtools: true
};
export default new Vuex.Store(store);

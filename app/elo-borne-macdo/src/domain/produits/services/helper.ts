import categorieInterface from "../interfaces/CategorieInterface";

export function isAvailableCategorie(
  categorie: string,
  categorieList: categorieInterface[]
) {
  return categorieList.filter(cat => cat.name === categorie).length > 0;
}

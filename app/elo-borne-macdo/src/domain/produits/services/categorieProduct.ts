import categorieInterface from "../interfaces/CategorieInterface";

export default function categorieList(): categorieInterface[] {
  const categories: categorieInterface[] = [
    {
      name: "NOS MENUS",
      products: [
        {
          name: "LE MENU BEST OF",
          description: "Lorem ipsum",
          price: 9.5,
          categorieSelected: "NOS MENUS"
        },
        {
          name: "LE MENU MAXI BEST OF",
          description: "Lorem ipsum",
          price: 9.5,
          categorieSelected: "NOS MENUS"
        }
      ]
    },
    {
      name: "NOS BURGERS",
      products: [
        {
          name: "Le royal delux",
          description: "Lorem ipsum",
          price: 8.5,
          categorieSelected: "NOS BURGERS"
        },
        {
          name: "LE ROYAL™ BACON",
          description: "Lorem ipsum",
          price: 9.0,
          categorieSelected: "NOS BURGERS"
        },
        {
          name: "LE ROYAL CHEESE™",
          description: "Lorem ipsum",
          price: 30,
          categorieSelected: "NOS BURGERS"
        }
      ]
    },
    {
      name: "LES SALADES DU JOUR",
      products: [
        {
          name: "LA CLASSIC CAESAR",
          description: "Lorem ipsum",
          price: 3.0,
          categorieSelected: "LES SALADES DU JOUR"
        },
        {
          name: "L'ITALIAN MOZZA ET PASTA",
          description: "Lorem ipsum",
          price: 4.0,
          categorieSelected: "LES SALADES DU JOUR"
        }
      ]
    },
    {
      name: "PETITE FAIM",
      products: [
        {
          name: "LE McMUFFIN™ EGG & CHEESE",
          description: "Lorem ipsum",
          price: 1.5,
          categorieSelected: "PETITE FAIM"
        },
        {
          name: "LE McMUFFIN™ EGG & BACON",
          description: "Lorem ipsum",
          price: 2.0,
          categorieSelected: "PETITE FAIM"
        }
      ]
    },
    {
      name: "NOS BOISSONS",
      products: [
        {
          name: "COCA-COLA®",
          description: "Lorem ipsum",
          price: 2.5,
          categorieSelected: "PETITE FAIM"
        },
        {
          name: "FANTA",
          description: "Lorem ipsum",
          price: 2.5,
          categorieSelected: "PETITE FAIM"
        }
      ]
    },
    {
      name: "NOS DESSERTS",
      products: [
        {
          name: "LE McFLURRY",
          description: "Lorem ipsum",
          price: 2.0,
          categorieSelected: "NOS DESSERTS"
        },
        {
          name: "LE SUNDAE",
          description: "Lorem ipsum",
          price: 2.5,
          categorieSelected: "NOS DESSERTS"
        },
        {
          name: "LE VERY PARFAIT",
          description: "Lorem ipsum",
          price: 0.5,
          categorieSelected: "NOS DESSERTS"
        }
      ]
    },
    {
      name: "NOS FRITES & SAUCES",
      products: [
        {
          name: "LES FRITES",
          description: "Lorem ipsum",
          price: 2.0,
          categorieSelected: "NOS FRITES & SAUCES"
        },
        {
          name: "LES DELUXE POTATOES",
          description: "Lorem ipsum",
          price: 2.5,
          categorieSelected: "NOS FRITES & SAUCES"
        },
        {
          name: "KETCHUP",
          description: "Lorem ipsum",
          price: 0.5,
          categorieSelected: "NOS FRITES & SAUCES"
        }
      ]
    },
    {
      name: "NOS SALADES",
      products: [
        {
          name: "LA CLASSIC CAESAR",
          description: "Lorem ipsum",
          price: 3.0,
          categorieSelected: "NOS SALADES"
        },
        {
          name: "L'ITALIAN MOZZA ET PASTA",
          description: "Lorem ipsum",
          price: 4.0,
          categorieSelected: "NOS SALADES"
        }
      ]
    }
  ];
  return categories;
}

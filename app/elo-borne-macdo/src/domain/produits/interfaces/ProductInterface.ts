import categorieInterface from "./CategorieInterface";

export default interface ProductInterface {
  name: string;
  price: number;
  description: string;
  categorieSelected: string;
}

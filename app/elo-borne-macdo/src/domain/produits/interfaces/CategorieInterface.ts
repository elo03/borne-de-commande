import ProductInterface from "./ProductInterface";

export default interface categorieInterface {
  name: string;
  products: ProductInterface[];
}

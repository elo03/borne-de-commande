import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "vue-material/dist/vue-material.min.css";
import "vue-material/dist/theme/default.css";
import {
  MdTabs,
  MdButton,
  MdContent,
  MdIcon,
  MdCard,
  MdField,
  MdChips,
  MdAutocomplete,
  MdMenu,
  MdList,
  MdSubheader,
  MdToolbar,
  MdBadge
} from "vue-material/dist/components";

Vue.use(MdTabs);
Vue.use(MdButton);
Vue.use(MdContent);
Vue.use(MdIcon);
Vue.use(MdCard);
Vue.use(MdField);
Vue.use(MdChips);
Vue.use(MdAutocomplete);
Vue.use(MdMenu);
Vue.use(MdList);
Vue.use(MdSubheader);
Vue.use(MdToolbar);
Vue.use(MdBadge);
Vue.config.productionTip = false;
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");

import categorieInterface from "@/domain/produits/interfaces/CategorieInterface";
import ProductInterface from "@/domain/produits/interfaces/ProductInterface";
import categorieList from "@/domain/produits/services/categorieProduct";

export default interface StoreInterface {
  productList: ProductInterface[];
  categories: categorieInterface[];
  editingProduct: ProductInterface;
}
